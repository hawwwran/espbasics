/// ESP Basics module ///
#include "ESPBasics.h"
//////

String firmwareVersion = "1.2.0";

namespace Webserver
{
  void Root() {
    ESPBasics::server.send(200, "text/plain", "Hello from firmware");
  }

  void APStop() {
    ESPBasics::server.send(200, "text/plain", "Shutting AP down.");
    delay(100);
    // STOP AP
    ESPBasics::apModeStop();
  }
}

void serialCommandHandler(String command, String value) {
  if(command == "whatever"){
      Serial.println("Whatever command called with value: " + value);
  }
}

void setup(void){
  /* Uncomment if you want to define your own 404 page
  ESPBasics::Settings::noDefault404 = true;
  */
  /* Uncomment if you dont't want to init FTP server
  ESPBasics::Settings::noFTP = true;
  */
  /* Uncomment if you dont't want to init HTTP server
  ESPBasics::Settings::noHTTP = true;
  */
  /* Uncomment if you dont't want to init OTA Update service
   * OTA Update service is accessible over HTTP on port 983
  ESPBasics::Settings::noUpdate = true;
  */
  /* Uncomment if you dont't want to be able to turn on AP mode
  ESPBasics::Settings::noAP = true;
  */
  // You can set custom Serial Command handler.
  ESPBasics::setSerialCommandHandler(serialCommandHandler);
  // Set firmware version (it's shown in update web interface)
  ESPBasics::setVersion(firmwareVersion);

  // SETUP START
  ESPBasics::setupStart();

  // AP MODE START. This should be called only after some owner's action. It could be pressing hardware button.
  ESPBasics::apModeStart();
  //
  
  // your setup code
  Serial.println("- Define URI listeners -");
  ESPBasics::server.on("/", Webserver::Root);
  ESPBasics::server.on("/apstop", Webserver::APStop);
  // end of your setup code

  // SETUP END
  ESPBasics::setupEnd();
}

void loop(void){
  ESPBasics::loop();
  // your loop code
  // end of your loop code
}
