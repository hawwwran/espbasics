/*
* WifiBasics Library
* ------------------
* version 1.2.0
*
* FTP server
* HTTP server
* Update server
* Multiple APs
* Can work as AP to be configurable over WiFi
* Configurable over Serial or over FTP (config files are saved in filesystem)
*
* Use <help> in serial to get information about available commands
* 
* https://bitbucket.org/hawwwran/espbasics
*/

#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266WebServer.h>
#include <ESP8266httpUpdate.h>
#include "ESP8266FtpServer.h"

namespace ESPBasics {
  const String basicsVersion = "1.2.0";
  
  FtpServer ftpSrv;
  ESP8266WebServer server(80);
  ESP8266WebServer updateServer(983);
  ESP8266WiFiMulti wifiMulti;
  void(*serialCommandCallback)(String, String);

  const char* ssid = "Airlive";
  const char* password = "abecedaabcd";

  void apModeStart();
  void apModeStop();
  
	namespace Settings {
		String ftpConfig = "/.ftpconf";
		String wifiConfig = "/.wificonf";
    String updateConfig = "/.updateconf";
    String apConfig = "/.apconf";
		bool noDefault404 = false;
		bool noFTP = false;
		bool noHTTP = false;
		bool noUpdate = false;
    bool noAP = false;

    String defaultAPPassword = "1234567890";
    String programVersion = "unset";
	}

  namespace Content {
    const String CSS = R"LECSS(
      body, html {
        padding: 0;
        margin: 0;
        background: #eee;
        font-size: 14px;
        font-family: Helvetica, Arial, sans-serif;
      }

      .fileContainer {
          overflow: hidden;
          position: relative;
      }
      
      .fileContainer [type=file] {
          cursor: inherit;
          display: block;
          font-size: 999px;
          filter: alpha(opacity=0);
          min-height: 100%;
          min-width: 100%;
          opacity: 0;
          position: absolute;
          right: 0;
          text-align: right;
          top: 0;
      }

      .subheading {
        font-size: 0.9em;
        color: #999;
        text-align: center;
        padding: 3px;
        margin-bottom: 10px;
      }

      .heading + .subheading {
        position: relative;
        top: -20px;
      }
      
      .heading {
        font-size: 2em;
        color: #333;
        text-align: center;
        font-weight: bold;
        margin-bottom: 0.5em;
        padding: 0.5em;
      }

      .heading--success {
        color: #279933;
      }

      .heading--warning {
        color: #a80303;
      }

      .button {
        outline: none;
        background-color: #279933;
        color: white;
        cursor: pointer;
        display: inline-block;
        font-size: 1.25em;
        font-weight: 700;
        max-width: 80%;
        overflow: hidden;
        padding: 0.625rem 1.25rem;
        text-overflow: ellipsis;
        white-space: nowrap;
        border: none;
      }

      .button:hover {
        background-color: #38d248;
      }

      p {
        color: #222;
        padding: 10px;
        margin: 0;
        font-size: 1em;
        text-align: center;
      }

      p a {
        color: #046e8c;
        text-decoration: underline;
      }

      p a:hover {
        color: #017b9e;
      }

      p a.button {
        outline: none;
        background-color: #279933;
        color: white;
        cursor: pointer;
        display: inline-block;
        font-size: 1.25em;
        font-weight: 700;
        max-width: 80%;
        overflow: hidden;
        padding: 0.625rem 1.25rem;
        text-overflow: ellipsis;
        white-space: nowrap;
        border: none;
        text-decoration: none;
      }

      p a.button:hover {
        background-color: #38d248;
      }

      .hidden {
        display: none;
      }
    )LECSS";

    String JS(){ 
      String x =
      "(function(){"
      " items = {};"
      " items['version'] = '" + basicsVersion + "';"
      " items['programVersion'] = '" + Settings::programVersion + "';"
      " function translate(str){"
      "   for(var key in items){ str = str.replace('{'+key+'}',items[key]); }"
      "   return str;"
      "}"
      " var elms = document.querySelectorAll('.translate');"
      " elms.forEach(function(elm){"
      "    elm.innerHTML = translate(elm.innerHTML);"
      " });"
      "})();";
      return x;
    }
    
    const String updateCSS = R"LECSS(
      .inputfile {
          width: 0.1px;
          height: 0.1px;
          opacity: 0;
          overflow: hidden;
          position: absolute;
          z-index: -1;
      }
      
      .inputfile + label {
          max-width: 80%;
          font-size: 1.25rem;
          font-weight: 700;
          text-overflow: ellipsis;
          white-space: nowrap;
          cursor: pointer;
          display: inline-block;
          overflow: hidden;
          padding: 0.625rem 1.25rem;
          outline: none;
      }
      
      .inputfile:focus + label,
      .inputfile.has-focus + label {
          outline: 1px dotted #000;
          outline: -webkit-focus-ring-color auto 5px;
      }
      
      .inputfile + label svg {
          width: 1em;
          height: 1em;
          vertical-align: middle;
          fill: currentColor;
          margin-top: -0.25em;
          margin-right: 0.25em;
      }

      .inputfile + label {
          color: white;
          background-color: #d3394c;
      }
      
      .inputfile:focus + label,
      .inputfile.has-focus + label,
      .inputfile + label:hover {
          outline: none;
          background-color: #722040;
      }

      .form-box {
        padding:10px;
        text-align: center;
      }

      .updating .button {
        display: none;
      }
      .updating .hidden {
        display: block;
      }
    )LECSS";

    const String fileInputEnhancementJS = R"LEJS(    
      (function(){
        var inputs = document.querySelectorAll( '.inputfile' );
        Array.prototype.forEach.call( inputs, function( input )
        {
          var label  = input.nextElementSibling,
            labelVal = label.innerHTML;
        
          input.addEventListener( 'change', function( e )
          {
            var fileName = '';
            if( this.files && this.files.length > 1 )
              fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
            else
              fileName = e.target.value.split( '\\' ).pop();
        
            if( fileName )
              label.querySelector( 'span' ).innerHTML = fileName;
            else
              label.innerHTML = labelVal;
          });
        });
      })();
    )LEJS";
    
    const String updateIndex = R"LEHTML(
    <link rel="stylesheet" type="text/css" href="/css/main.css" />
    <link rel="stylesheet" type="text/css" href="/css/update.css" />
    
    <div id="updateBox">
      <h1 class="heading">Update device's firmware</h1>
      <h2 class="subheading translate">Firmware version: {programVersion} | ESPBasics version: {version}</h2>
      <form method='POST' action='/update' enctype='multipart/form-data' target="leframe">
        <div class="form-box">
          <input type="file" name="update" id="file" class="inputfile" accept=".bin" />
          <label for="file"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Choose a file with firmware image</span></label>
        </div>
        <div class="form-box" id="submitBox">
          <input type="submit" value="Update" class="button" onclick="doTheUpdate()"/>
          <p class="hidden">Uploading firmware. Please wait.</p>
        </div>
      </form>
    </div>
    <div id="updateFinishedBox" class="hidden">
      <h1 class="heading heading--success">Update finished successfully. Device is restarting...</h1>
      <h2 class="subheading">Manual restart may be necessary.</h2>
    </div>
    <div id="updateFailedBox" class="hidden">
      <h1 class="heading heading--warning">Update failed</h1>
      <p><a class="button" href='/'>Try it again</a></p>
    </div>
    <iframe src="about:blank" class="hidden" name="leframe" id="leframe"></iframe>
    <script src="/js/main.js"></script>
    <script src="/js/update.js"></script>
    <script>
      function doTheUpdate() {
        document.getElementById('submitBox').classList.add('updating');

        var frame = document.getElementById('leframe');
        var intrvl = setInterval(function() {
          var frameDocument = frame.contentDocument || frame.contentWindow && frame.contentWindow.document;
          if(frameDocument){
            var lebody = frameDocument.getElementsByTagName("body")[0];
            if(lebody){
              if(lebody.innerText == "OK"){
                clearInterval(intrvl);
                document.getElementById("updateBox").classList.add("hidden");
                document.getElementById("updateFinishedBox").classList.remove("hidden");

                frame.setAttribute("src","/?hello=true");
                var intrvlhello = setInterval(function() {
                  console.log("Checking...");
                  try {
                    var frameDocument = frame.contentDocument || frame.contentWindow && frame.contentWindow.document;
                  } catch(e) {
                    frame.setAttribute("src","/?hello=true");
                  }
                  if(frameDocument){
                    var lebody = frameDocument.getElementsByTagName("body") && frameDocument.getElementsByTagName("body")[0];
                    if(lebody){
                      if(lebody.innerText == "ehlo"){
                        clearInterval(intrvlhello);
                       location.reload();
                      } else {
                        frame.setAttribute("src","/?hello=true");
                      }
                    }
                  }
                }, 5000);
              } else if(lebody.innerText != "") {
                clearInterval(intrvl);
                document.getElementById("updateBox").classList.add("hidden");
                document.getElementById("updateFailedBox").classList.remove("hidden");
              }
            }
          }
        }, 1000);
      }
    </script>
    )LEHTML";

    const String updateFinished = R"LEHTML(
    <h1 class="heading heading--success">Update finished successfully. Device is restarting...</h1>
    )LEHTML";

    const String updateNotSet = R"LEHTML(
      <h1 class="heading">Update server not set</h1>
      <p>Use serial monitor and command &lt;setupdate:PASSWORD&gt;</p>
    )LEHTML";

    const String updateNotLogged = R"LEHTML(
      <h1 class="heading heading--warning">Invalid password</h1>
      <p><a class="button" href='/'>Try it again</a></p>
    )LEHTML";
  }

	namespace Tools {
    String getAPName(){
      uint32_t chipId = ESP.getChipId();
      char uuid[64];
      
      sprintf_P(uuid, PSTR("%02x%02x%02x"),
            (uint16_t) ((chipId >> 16) & 0xff),
            (uint16_t) ((chipId >>  8) & 0xff),
            (uint16_t)   chipId        & 0xff);

      String s(uuid);
      return s;
    }
    
		void fileCreate(String name) {
			File f = SPIFFS.open(name, "a");
			if (!f) {
				Serial.print("File creation/opening failed: ");
				Serial.println(name);
				return;
			}
			f.close();
		}

		void fileAppend(String name, String str) {
			File f = SPIFFS.open(name, "a");
			if (!f) {
				Serial.print("File creation/opening failed: ");
				Serial.println(name);
				return;
			}

			f.println(str);
			f.close();
		}

		void fileWrite(String name, String str) {
			File f = SPIFFS.open(name, "w");
			if (!f) {
				Serial.print("File creation/opening failed: ");
				Serial.println(name);
				return;
			}

			f.println(str);
			f.close();
		}

		String readFirstLine(String filename, bool ignoreIfNotExist = false) {
			File f = SPIFFS.open(filename, "r");
			if (!f) {
        if(!ignoreIfNotExist){
  				Serial.print("File opening failed: ");
  				Serial.println(filename);
        }
				return "";
			}

			while (f.available()) {
				String line = f.readStringUntil('\n');
				line.trim();
				return line;
			}

			f.close();
		}

		String getNthPart(String str, int n, char separator = ';') {
			int len = str.length();
			int count = 0;
			int state = (n == 0 ? 1 : 0);
			char c;
			String result = "";

			for (int i = 0; i < len; i++) {
				c = str[i];
				switch (state) {
				case 0:
					if (c == separator) {
						count++;
						if (count == n) {
							state = 1;
						}
					}
					break;
				case 1:
					if (c == separator || c == 13 || c == 10) {
						return result;
					}
					else {
						result += c;
					}
					break;
				}
			}

			return result;
		}
	}

	namespace UpdateServer {
	  void notSet() {
      updateServer.sendHeader("Connection", "close");
      updateServer.sendHeader("Access-Control-Allow-Origin", "*");
      updateServer.send(200, "text/html", 
        Content::updateNotSet +
        Content::CSS
      );
    }

    void notLogged() {
      updateServer.sendHeader("Connection", "close");
      updateServer.sendHeader("Access-Control-Allow-Origin", "*");
      updateServer.send(401, "text/html", 
        Content::updateNotLogged +
        Content::CSS
      );
    }
    
    bool isSet() {
      File f = SPIFFS.open(Settings::updateConfig, "r");
      if (f) {
        return true;
      }

      return false;
    }

    bool isLogged(bool ignoreAuth = false) {
      String updateCredentials = Tools::readFirstLine(Settings::updateConfig);
      updateCredentials.trim();
      String sUsername = Tools::getNthPart(updateCredentials, 0);
      String sPassword = Tools::getNthPart(updateCredentials, 1);
      char username[sUsername.length()+1];
      char password[sPassword.length()+1];

      sUsername.toCharArray(username, sUsername.length()+1);
      sPassword.toCharArray(password, sPassword.length()+1);

      if(!updateServer.authenticate(username, password)) {
        if(!ignoreAuth) {
          updateServer.requestAuthentication(); 
        }
        return false;
      }
      
      return true;
    }

		void onRoot() {
      if(updateServer.arg("hello") == "true"){
        updateServer.sendHeader("Connection", "close");
        updateServer.sendHeader("Access-Control-Allow-Origin", "*");
        updateServer.send(200, "text/plain", "ehlo"); 
        return;
      }
    
      if(!isSet()) {
        notSet();
        return;
      }

      if(isLogged()){
        updateServer.sendHeader("Connection", "close");
        updateServer.sendHeader("Access-Control-Allow-Origin", "*");

        updateServer.send(200, "text/html", 
          (updateServer.arg("finished") != NULL ? Content::updateFinished : "") +
          Content::updateIndex
        );
      }
		}

    void onUpdate() {
      if(!isSet()) {
        notSet();
        return;
      }

      if(!isLogged(true)) {
        notLogged();
        return;
      }

      updateServer.sendHeader("Connection", "close");
      updateServer.sendHeader("Access-Control-Allow-Origin", "*");
      if(Update.hasError()) {
        updateServer.send(500, "text/plain", "FAIL");
      } else {
        updateServer.send(200, "text/plain", "OK");
      }
      ESP.reset();
    }

    void onFileUpload() {
      if(!isLogged(true)){
        return;
      }
      
      HTTPUpload& upload = updateServer.upload();
      if(upload.status == UPLOAD_FILE_START){
        Serial.setDebugOutput(true);
        Serial.printf("Update: %s\n", upload.filename.c_str());
        uint32_t maxSketchSpace = (ESP.getFreeSketchSpace() - 0x1000) & 0xFFFFF000;
        if(!Update.begin(maxSketchSpace)){//start with max available size
          Update.printError(Serial);
        }
      } else if(upload.status == UPLOAD_FILE_WRITE){
        if(!isLogged(updateServer.arg("password"))) {
          return;
        }
        if(Update.write(upload.buf, upload.currentSize) != upload.currentSize){
          Update.printError(Serial);
        }
      } else if(upload.status == UPLOAD_FILE_END){
        if(!isLogged(updateServer.arg("password"))) {
          return;
        }
        if(Update.end(true)){ //true to set the size to the current progress
          Serial.printf("Update Success: %u\nRebooting...\n", upload.totalSize);
        } else {
          Update.printError(Serial);
        }
        Serial.setDebugOutput(false);
      }
      yield();
    }

    void fileCSS_main(){
      updateServer.send(200, "text/css", Content::CSS);
    }

    void fileCSS_update(){
      updateServer.send(200, "text/css", Content::updateCSS);
    }

    void fileJS_main(){
      updateServer.send(200, "text/css", Content::JS());
    }

    void fileJS_update(){
      updateServer.send(200, "text/css", Content::fileInputEnhancementJS);
    }

    void handleNotFound() {
      updateServer.send(404);
    }
	}

	namespace Command {
		void removeWifi(String ssid, bool silent = false) {
			if (!silent) Serial.println("- Remove Wifi -");

			File f = SPIFFS.open(Settings::wifiConfig, "r");
			if (f) {
				File tmp = SPIFFS.open(Settings::wifiConfig + ".tmp", "w");
				if (tmp) {
					while (f.available()) {
						String line = f.readStringUntil('\n');
						line.trim();
						if (Tools::getNthPart(line, 0) != ssid) {
							tmp.println(line);
						}
						else if (silent) {
							Serial.println("- AP config will be replaced");
						}
					}
					tmp.close();
					f.close();
					SPIFFS.remove(Settings::wifiConfig);
					SPIFFS.rename(Settings::wifiConfig + ".tmp", Settings::wifiConfig);

					if (!silent) Serial.println("- Wifi AP removed");
				}
				else {
					Serial.println("! Cannot create temp file !");
				}
				if (f) {
					f.close();
				}
			}
			else {
				Serial.println("! Cannot read config file !");
			}

			if (!silent) {
				Serial.println("-- Done");
				Serial.println();
			}
		}

		void addWifi(String value) {
			Serial.println("- Add Wifi -");
			String line = "";

			removeWifi(Tools::getNthPart(value, 0), true);

			line += Tools::getNthPart(value, 0);
			line += ";";
			line += Tools::getNthPart(value, 1);
			line += ";";

			Tools::fileAppend(Settings::wifiConfig, line);

			Serial.println("-- Done");
			Serial.println();
		}

		void wifiList() {
			Serial.println("- List of wifi APs -");
			Tools::fileCreate(Settings::wifiConfig);

			File f = SPIFFS.open(Settings::wifiConfig, "r");
			if (f) {
				while (f.available()) {
					String line = f.readStringUntil('\n');
					const char * ssid;
					const char * password;
					String str_ssid = Tools::getNthPart(line, 0);
					String str_password = Tools::getNthPart(line, 1);

					ssid = str_ssid.c_str();
					password = str_password.c_str();

					Serial.print("SSID: ");
					Serial.print(ssid);
					Serial.print(" / Password: ");
					Serial.println(password);
				}
				f.close();
			}
			else {
				Serial.println("! Cannot read config file !");
			}

			Serial.println("-- Done");
			Serial.println();
		}

    void scanWifi() {
      Serial.println("- Scan for WiFi networks -");
      
      int n = WiFi.scanNetworks();
      Serial.println("Scan done");
      if (n < 0) {
        Serial.println("Error while scanning");
      } else if (n == 0)
        Serial.println("No networks found");
      else
      {
        Serial.print(n);
        if(n == 1){
          Serial.println(" network found");
        } else {
          Serial.println(" networks found");
        }
        for (int i = 0; i < n; ++i)
        {
          // Print SSID and RSSI for each network found
          Serial.print(i + 1);
          Serial.print(": ");
          Serial.print(WiFi.SSID(i));
          Serial.print(" (");
          Serial.print(WiFi.RSSI(i));
          Serial.print(")");
          Serial.println((WiFi.encryptionType(i) == ENC_TYPE_NONE)?" ":"*");
          delay(10);
        }
      }
      Serial.println("");
    }

		void setFTP(String value) {
			Serial.println("- Set FTP -");
			String line = "";

			line += Tools::getNthPart(value, 0);
			line += ";";
			line += Tools::getNthPart(value, 1);
			line += ";";

			Tools::fileWrite(Settings::ftpConfig, line);

			Serial.println("-- Done");
			Serial.println();
		}

    void setUpdate(String value) {
      Serial.println("- Set Update -");

      value = Tools::getNthPart(value, 0) + ";" + Tools::getNthPart(value, 1);
      
      Tools::fileWrite(Settings::updateConfig, value);

      Serial.println("-- Done");
      Serial.println();
    }

    void setAP(String value) {
      Serial.println("- Set AP -");

      value = Tools::getNthPart(value, 0) + ";" + Tools::getNthPart(value, 1);
      
      Tools::fileWrite(Settings::apConfig, value);

      Serial.println("-- Done");
      Serial.println();
    }

		void activeConnection() {
			Serial.println("- Active Connection -");
			Serial.print("IP: ");
			Serial.println(WiFi.localIP());
			Serial.print("SSID: ");
			Serial.println(WiFi.SSID());
			Serial.println();
		}
	}

	/////////////////
	/////////////////
	/////////////////
  void setVersion(String programVersion) {
    Settings::programVersion = programVersion;
  }

	void setSerialCommandHandler(void(*cb)(String, String)) {
		serialCommandCallback = cb;
	}

	void defaultHandleNotFound() {
		String message = "File Not Found\n\n";
		message += "URI: ";
		message += server.uri();
		message += "\nMethod: ";
		message += (server.method() == HTTP_GET) ? "GET" : "POST";
		message += "\nArguments: ";
		message += server.args();
		message += "\n";
		for (uint8_t i = 0; i < server.args(); i++) {
			message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
		}
		server.send(404, "text/plain", message);
	}

	void handleSerialCommand(String command, String value) {
		Serial.println();

		if (serialCommandCallback) {
			Serial.println("- Custom serial command handler prepared -");
			serialCommandCallback(command, value);
		}

		if (command == "wifiadd") {
			Command::addWifi(value);
			wifiMulti.addAP(Tools::getNthPart(value, 0).c_str(), Tools::getNthPart(value, 1).c_str());
		}
		else if (command == "wifilist") {
			Command::wifiList();
		}
		else if (command == "wifiremove") {
			Command::removeWifi(value);
		}
    else if (command == "wifiscan") {
      Command::scanWifi();
    }
		else if (command == "connection") {
			Command::activeConnection();
		}
		else if (command == "setftp") {
			if (!Settings::noFTP) {
				Command::setFTP(value);
				Serial.println("- Init FTP -");
				ftpSrv.begin(Tools::getNthPart(value, 0), Tools::getNthPart(value, 1));
			}
			else {
				Serial.println("! FTP is turned off in firmware !");
			}
		}
    else if (command == "setupdate") {
     if (!Settings::noUpdate) {
        Command::setUpdate(value);
      } else {
        Serial.println("! Update server is turned off in firmware !");
      }
    }
    else if (command == "setap") {
     if (!Settings::noAP) {
        if(value.indexOf(";") >= 0){
          if(Tools::getNthPart(value,1).length() < 8 && Tools::getNthPart(value,1).length() > 0){
            Serial.println("Password cannot be shorter than 8 characters");
          } else {
            Command::setAP(value);
          }
        } else {
          if(value.length() < 8 && value.length() > 0){
            Serial.println("Password cannot be shorter than 8 characters");
          } else {
            Command::setAP(";" + value);
          }
        }
      } else {
        Serial.println("! AP is disabled in firmware !");
      }
    }
    else if (command == "startap") {
      apModeStart();
    }
    else if (command == "stopap") {
      apModeStop();
    }
    else if (command == "restart") {
      Serial.println("-- Restarting --");
      ESP.restart();
    }
		else if (command == "help") {
			Serial.println();
			Serial.println("- Help -");
			Serial.println("--------");
			Serial.println("<help> - This help");
      Serial.println("<restart> - Restart device");
			Serial.println("<wifiadd:SSID;PASSWORD> - Add Wifi AP. Device will connect to the more powerfull AP available. If the AP was defined, it's password is rewritten.");
      Serial.println("<wifiremove:SSID> - Removes wifi AP if found in config file.");
      Serial.println("<wifilist> - Prints list of all APs device have defined.");
      Serial.println("<wifiscan> - Finds available WiFi networks");
			if (!Settings::noFTP) {
				Serial.println("<setftp:USERNAME;PASSWORD> - Set username and password for FTP. Only one user can be defined. Set will overwrite previous setting.");
			}
      if (!Settings::noUpdate) {
       Serial.println("<setupdate:USERNAME;PASSWORD> - Set username and password for Update service. Set will overwrite previous setting. OTA Update service is accessible over HTTP on port 983.");
      }
      if (!Settings::noAP) {
       Serial.println("<setap:SSID;PASSWORD> / <setap:PASSWORD> - Set SSID and password for AP. SSID is optional. If not set, default SSID will be used AP must be started by firmware.");
       Serial.println("<startap> - Starts AP");
       Serial.println("<stopap> - Stops AP");
      }
			Serial.println("--------");
			Serial.println();
		}
	}

	int state = 0;
	String command = "";
	String value = "";

	void readCommandFromSerial() {
		if (Serial.available() > 0) {
			while (Serial.available() > 0) {
				char c = Serial.read();  //gets one byte from serial buffer
				switch (state) {
				case 0:
					if (c == '<') {
						state = 1;
					}
					break;
				case 1:
					if (c == ':' || c == '>' || c == 10) {
						if (c == '>' || c == 10) {
							handleSerialCommand(command, value);
							state = 0;
						}
						else {
							state = 2;
						}
					}
					else {
						command += c;
					}
					break;
				case 2:
					if (c == '>' || c == 10) {
						state = 0;
						handleSerialCommand(command, value);
					}
					else {
						value += c;
					}
					break;
				}

				if (state == 0) {
					command = "";
					value = "";
				}
			}
		}
	}

	void reconnect() {
		Serial.println("");
		Serial.println("- Connecting -");

		int i = 0;
		while (wifiMulti.run() != WL_CONNECTED) {
			if (i == 40) {
				Serial.println();
				i = 0;
			}
			i++;

			Serial.print(".");
			delay(500);

			// Handle Serial commands
			readCommandFromSerial();
		}

		Serial.println("");
		Serial.println("");
		Serial.println("- WiFi connected -");
		Serial.print("IP address: ");
		Serial.println(WiFi.localIP());
		Serial.print("SSID: ");
		Serial.println(WiFi.SSID());
		Serial.println("");

		delay(100);
	}

	char* string2char(String command) {
		if (command.length() != 0) {
			char *p = const_cast<char*>(command.c_str());
			return p;
		}
	}

	void addAPs() {
		Serial.println("- Add Wifi APs from config -");

		int count = 0;
		Tools::fileCreate(Settings::wifiConfig);
		File f = SPIFFS.open(Settings::wifiConfig, "r");
		if (f) {
			while (f.available()) {
				count++;
				String line = f.readStringUntil('\n');
				const char * ssid;
				const char * password;
				String str_ssid = Tools::getNthPart(line, 0);
				String str_password = Tools::getNthPart(line, 1);

				ssid = str_ssid.c_str();
				password = str_password.c_str();

				Serial.print("SSID: ");
				Serial.println(ssid);

				wifiMulti.addAP(ssid, password);
			}

			if (count == 0) {
				Serial.println("! Config empty. No APs used. !");
				Serial.println("Add AP to connect to and reboot the device.");
				Serial.println("Use command: <addwifi:SSID;PASSWORD>");
			}

			f.close();
		}
		else {
			Serial.println("! Cannot read config file !");
		}

		Serial.println();
	}

  void apModeStop() {
    Serial.println("- AP stopped -");
    WiFi.mode(WIFI_STA);
  }
  
  void apModeStart(){
    Serial.println("- Start AP -");
    WiFi.mode(WIFI_AP_STA);
  
    String apCredentials = Tools::readFirstLine(Settings::apConfig,true);
    if(apCredentials == "" || apCredentials == ";"){
      String defSSID = "ESPAP_" + Tools::getAPName() + "_default";
      
      Serial.println("! AP is in default mode. Use <help> to see how to set AP. !");
      apCredentials = defSSID + ";" + Settings::defaultAPPassword;
      Serial.println("SSID: " + defSSID);
      Serial.println("PWD: " + Settings::defaultAPPassword);
    }
  
    String apName = Tools::getNthPart(apCredentials, 0);
    apName = (apName == "" ? "ESPAP_" + Tools::getAPName() : apName);
    char ch_apName[apName.length()+1];
    apName.toCharArray(ch_apName, apName.length() + 1);
    
    String apPassword = Tools::getNthPart(apCredentials, 1);
    if(apPassword == ""){ 
      Serial.println("! Default password used to access AP !");
      Serial.println("! Use <help> to see how to set AP !");
      apPassword = Settings::defaultAPPassword;
    }
    char ch_apPassword[apPassword.length()+1];
    apPassword.toCharArray(ch_apPassword, apPassword.length() + 1);

    Serial.print("- AP name: ");
    Serial.println(ch_apName);
    WiFi.softAP(ch_apName, ch_apPassword);
  
    IPAddress myIP = WiFi.softAPIP();
    Serial.print("- AP IP address: ");
    Serial.println(myIP);
  }

	void setupStart() {
		// Init serial
		Serial.begin(115200);
		delay(10);
		Serial.println();
		Serial.println();
		Serial.println("-------------------------------");
		Serial.println("-- Initialize basic services --");
    Serial.println("-- Firmware version: " + Settings::programVersion);
    Serial.println("-- ESPBasics version: " + basicsVersion);
		Serial.println("-------------------------------");
		Serial.println();

		if (SPIFFS.begin()) {
			Serial.println("- Filesystem ready -");
		}

		addAPs();

		// Init HTTP server
		if (!Settings::noDefault404 && !Settings::noHTTP) server.onNotFound(defaultHandleNotFound);

		// Init Update server
		if (!Settings::noUpdate) {
			updateServer.on("/", UpdateServer::onRoot);
      updateServer.on("/update", HTTP_POST, UpdateServer::onUpdate, UpdateServer::onFileUpload);
      updateServer.on("/css/main.css", UpdateServer::fileCSS_main);
      updateServer.on("/css/update.css", UpdateServer::fileCSS_update);
      updateServer.on("/js/main.js", UpdateServer::fileJS_main);
      updateServer.on("/js/update.js", UpdateServer::fileJS_update);
			updateServer.onNotFound(UpdateServer::handleNotFound);
		}

    WiFi.mode(WIFI_STA);
	}

	void setupEnd() {
    reconnect();
  
    if (!Settings::noUpdate) {
      updateServer.begin();
      Serial.println("- Update server started -");
    }
    
		if (!Settings::noHTTP) {
			server.begin();
			Serial.println("- HTTP server started -");
		}
		// Init FTP server
		if (!Settings::noFTP) {
			String ftpCredentials = Tools::readFirstLine(Settings::ftpConfig);
			ftpSrv.begin(Tools::getNthPart(ftpCredentials, 0), Tools::getNthPart(ftpCredentials, 1));
			Serial.println("- FTP server started -");
		}

		Serial.println();
		Serial.println("---------------------------------");
		Serial.println("---- Initialization finished ----");
		Serial.println("---------------------------------");
		Serial.println();
	}

	void loop() {
		// Handle Serial commands
		readCommandFromSerial();
		// Handle FTP server
		if (!Settings::noFTP) {
			ftpSrv.handleFTP();
		}
		// Handle webserver
		server.handleClient();

    // Handle update server
    if (!Settings::noUpdate) {
      updateServer.handleClient();
    }
    
		// Hancle Wifi connection
		if (wifiMulti.run() != WL_CONNECTED) {
			Serial.println("WiFi disconnected!");
			reconnect();
		}
	}
}
