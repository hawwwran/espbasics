# ESPBasics library #

### What is this library for? ###

* This library takes care of basic services. Configurable over serial interface, allowing to set more APs to connect to (program chooses the best network available), contains FTP, HTTP, and OTA Update server. Confiugration is saved in files on filesystem. Allows to extend Serial command interface.
* 1.0
* 1.1.0 - OTA Update server added

### How do I set it up? ###

* Download library folder
* Place it in you arduino Library folder
* Include in your project file

```
#!arduino

/// ESP Basics module ///
#include "ESPBasics.h"
//////
```

* Place default setup initialization in your setup method

```
#!arduino

ESPBasics::setupStart();
// Here goes your setup code
ESPBasics::setupEnd();
```

* Place default loop code in your loop method

```
#!arduino

ESPBasics::loop();
// your loop code
```

* Build and push in your ESP module
* Open serial monitor and send **<help>** to your module.
* Set up your device according to help
* Profit

### How to turn off some service? ###

Services are by default turned on. If you want to turn off some of the services, call one of these before ESPBasics::setupStart();


```
#!arduino

// Use this if you want to define your own 404 page
ESPBasics::Settings::noDefault404 = true;

// Use this if you dont't want to init FTP server
ESPBasics::Settings::noFTP = true;

// Use this if you dont't want to init HTTP server
ESPBasics::Settings::noHTTP = true;

// Use this if you dont't want to init OTA Update service
ESPBasics::Settings::noUpdate = true;
  
```

### Serial-Command interface ###
You can send commands over serial interface to set services or do other stuff.

* use <help> to see all possible commands 

```
#!arduino

- Help -
--------
<help> - This help
<restart> - Restart device
<wifiadd:SSID;PASSWORD> - Add Wifi AP. Device will connect to the more powerfull AP available. If the AP was defined, it's password is rewritten.
<wifiremove:SSID> - Removes wifi AP if found in config file.
<wifilist> - Prints list of all APs device have defined.
<wifiscan> - Finds available WiFi networks
<setftp:USERNAME;PASSWORD> - Set username and password for FTP. Only one user can be defined. Set will overwrite previous setting.
<setupdate:USERNAME;PASSWORD> - Set username and password for Update service. Set will overwrite previous setting. OTA Update service is accessible over HTTP on port 983.
<setap:SSID;PASSWORD> / <setap:PASSWORD> - Set SSID and password for AP. SSID is optional. If not set, default SSID will be used AP must be started by firmware.
<startap> - Starts AP
<stopap> - Stops AP
--------
```




### HTTP Server ###
Basic HTTP server on port 80. URI Handlers need to be defined by you between setupStart and setupEnd. HTTP server object is accessible in variable "server".
```
#!arduino

ESPBasics::setupStart();
ESPBasics::server.on("/", rootHandler);
ESPBasics::server.on("/somefolder", [](){
  // Inline handler function
});
ESPBasics::setupEnd();
```

### FTP Server ###
* Serial command to set username and password: **<setftp:USERNAME;PASSWORD>**
FTP server allows only one user to have access and only one connection at a time. Device uses SPIFFS. That means no folders. Keep that in mind.

### OTA Update server ###
* Serial command to set username and password: **<setupdate:USERNAME;PASSWORD>**
Update server listens on port 983 and requires basic HTTP auth to show interface for updating device.

* Upload only valid .bin files!
* Device is told to restart automatically after update, but additional manual restart may be required.
* Use ESPBasics::setVersion(String version); to set version of firmware. This version i s shown in serial console on boot and in update web interface.

![update.jpg](https://bitbucket.org/repo/RXz8ja/images/2193757318-update.jpg)

### Multiple AP feature ###
This library provides ability to connect to multiple AP's. Only one at a time, but in case of lost signal, the device will try to reconnect to the most powerful network known and available.

* Serial command to add credentials to wifi network: **<wifiadd:SSID;PASSWORD>**
* Serial command to remove credentials to wifi network: **<wifiremove:SSID>**
* Serial command to list all known network credentials: **<wifilist>**
* Serial command to scan all available networks: **<wifiscan>**

### AP mode ###
The library provides possibility to turn on AP (to create WiFi network). The AP and client mode can be both active at the same time. The AP mode s by default used to allow to access update server, when it's not possible to connect the device to network.


```
#!arduino

//Start AP
ESPBasics::apModeStart();
```

```
#!arduino

//Stop AP
ESPBasics::apModeStop();
```


* The default SSID is ESPAP_[CHIP-ID]_default.
* Once the password is set (using <setap:PASSWORD>), the "_default" is ommited
* Use <setap:PASSWORD> to keep default SSID, but set password to have access secured
* Use <setap:SSID;PASSWORD> to set both, SSID and PASSWORD

### Any question? ###

* espbasics@hawwwran.com